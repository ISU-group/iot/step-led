#include "Config.h"
#include "WiFi.h"
#include "MQTT.h"
#include "Range.h"

void blink(int);

void setup() {
  Serial.begin(9600);

  pinMode(LED_BUILTIN, OUTPUT);
  blink(3);

  WIFI_init(false);
  MQTT_init();
  range_init();
}

void loop() {
  mqtt_cli.loop();

  mqtt_cli.publish(TOPIC_SEND_RANGE, String(round(range())).c_str());
  delay(750);
//  delay(2000);
}

void blink(int n) {
  for (int i = 0; i < n; i++) {
    digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
    delay(500);
  }
}
