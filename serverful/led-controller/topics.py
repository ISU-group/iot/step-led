class Topics:
    _RANGE_SENSOR = "lab/leds/strip/range"
    _LED_STRIP = "lab/leds/strip/set_leds_bytes"

    def _create_topic(base, idx):
        return f"{base}/{idx}"


    def range_sensor(idx):
        return Topics._create_topic(Topics._RANGE_SENSOR, idx)

    
    def led_strip(idx):
        return Topics._create_topic(Topics._LED_STRIP, idx)