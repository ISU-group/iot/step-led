import paho.mqtt.client as paho
from random import randint

import config as cfg
from leds import UltraSonicDistanceSensor, LedStripController
from topics import Topics


print(f"Hello, {cfg.CLIENT_ID}")

led_strip_controller: LedStripController = None


def random_colors(num_leds):
    return [randint(0, 255) for _ in range(num_leds * 3)]


def on_message(client, userdata, message):
    data = float(message.payload.decode("utf-8"))
    sensor_id = int(str(message.topic).split('/')[-1])

    prev_led, new_led = led_strip_controller.update_sensor(sensor_id, data)

    if prev_led == new_led:
        return

    is_active_strip = led_strip_controller.is_active_strip(prev_led)

    if prev_led is not None and not is_active_strip:
        led_strip_controller.black_strip(client, prev_led)

    if new_led is not None and not is_active_strip:
        # led_strip_controller.white_strip(client, new_led)
        led_strip_controller.set_strip(client, new_led, random_colors(cfg.NUM_LEDS))
    # print(f"Leds: prev = {prev_led}\tnew_led = {new_led}", end="\n"*2)


if __name__ == "__main__":
    client = paho.Client(cfg.CLIENT_ID)

    client.on_message = on_message

    print(f"Connecting to {cfg.BROKER}")
    client.connect(cfg.BROKER)

    sensors = []
    for idx in range(1, cfg.NUM_SENSORS + 1):
        sensors.append(UltraSonicDistanceSensor(idx))
        client.subscribe(Topics.range_sensor(idx))

    lhs_sensor, rhs_sensor = sensors[0], sensors[1]

    led_strip_controller = LedStripController(
        lhs_sensor, rhs_sensor,
        max_range=cfg.MAX_RANGE,
        led_strip_num=cfg.NUM_LED_STRIP
    )

    led_strip_controller.white_all(client, delay=0.35)
    led_strip_controller.black_all(client, delay=0.35)

    print(f"Starting now!")
    client.loop_forever()
