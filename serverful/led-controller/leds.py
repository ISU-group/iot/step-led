import paho.mqtt.client as paho
import time

import config as cfg
from topics import Topics


class UltraSonicDistanceSensor:
    
    def __init__(self, sensor_id) -> None:
        self.id = sensor_id
        self.distance = None

    def update(self, distance):
        self.distance = distance


class LedStripController:
    
    def __init__(
        self, 
        lhs_sensor, 
        rhs_sensor,
        max_range=cfg.MAX_RANGE,
        led_strip_num=cfg.NUM_LED_STRIP,
        leds_number=cfg.NUM_LEDS
    ) -> None:
        self.lhs_sensor = lhs_sensor
        self.rhs_sensor = rhs_sensor

        self._sensors = {
            lhs_sensor.id: lhs_sensor,
            rhs_sensor.id: rhs_sensor
        }

        self.max_range = max_range
        self.led_strip_num = led_strip_num
        self._led_strip_interval = max_range / (led_strip_num + 1)
        self.leds_number = leds_number


    @property
    def rgb_led_number(self):
        return self.leds_number * 3


    def update_sensor(self, sensor_id, data):
        prev_led_strip_id = self._find_led(sensor_id)
        
        self._sensors[sensor_id].update(data)

        new_led_strip_id = self._find_led(sensor_id)

        return (prev_led_strip_id, new_led_strip_id)


    def _find_led(self, sensor_id):
        # print(f"Finding led for {sensor_id} sensor")
        if sensor_id == self.lhs_sensor.id and self.lhs_sensor.distance is not None:
            return round(self.lhs_sensor.distance / self._led_strip_interval)
        elif sensor_id == self.rhs_sensor.id and self.rhs_sensor.distance is not None:
            return self.led_strip_num - round(self.rhs_sensor.distance / self._led_strip_interval) + 1
        return None


    def set_strip(
        self, 
        mqtt_client: paho.Client,
        led_strip_id,
        strip_intensity = []
    ):
        if len(strip_intensity) == 0:
            return

        strip_intensity = strip_intensity[:self.rgb_led_number]

        topic = Topics.led_strip(led_strip_id)
        mqtt_client.publish(topic, bytearray(strip_intensity))

        # print(f"Set led strip # {led_strip_id}")


    def black_strip(
        self,
        mqtt_client: paho.Client,
        led_strip_id,
    ):
        strip_intensity = [0, 0, 0] * self.rgb_led_number
        self.set_strip(mqtt_client, led_strip_id, strip_intensity)


    def white_strip(
        self,
        mqtt_client: paho.Client,
        led_strip_id
    ):
        strip_intensity = [255, 255, 255] * self.rgb_led_number
        self.set_strip(mqtt_client, led_strip_id, strip_intensity)


    def black_all(self, mqtt_client: paho.Client, delay=0):
        for i in range(1, cfg.NUM_LED_STRIP + 1):
            self.black_strip(mqtt_client, i)
            time.sleep(delay)


    def white_all(self, mqtt_client: paho.Client, delay=0):
        for i in range(1, cfg.NUM_LED_STRIP + 1):
            self.white_strip(mqtt_client, i)
            time.sleep(delay)

    
    def is_active_strip(self, strip_id):
        for sensor_id in self._sensors:
            active_strip = self._find_led(sensor_id)

            if strip_id == active_strip:
                return True
        return False