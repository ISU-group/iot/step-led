#pragma once

#include <PubSubClient.h>
#include "Config.h"

PubSubClient mqtt_cli(wifiClient);

void MQTT_init(){
  mqtt_cli.setServer(MQTT_BROKER, MQTT_PORT);
  mqtt_cli.setBufferSize(2048);
//  mqtt_cli.setCallback(callback);
  while (!mqtt_cli.connected()) {
      String client_id = "esp8266-" + String(WiFi.macAddress());
      Serial.print("The client " + client_id);
      Serial.println(" connects to the public mqtt broker\n");
      if (mqtt_cli.connect(client_id.c_str())){
          Serial.println("MQTT Connected");
      } else {
          Serial.print("failed with state ");
          Serial.println(mqtt_cli.state());
          delay(2000);
      }
  }  
}
