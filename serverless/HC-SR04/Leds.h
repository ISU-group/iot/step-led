#include "Config.h"

#include <FastLED.h>

int find_led(float dist) {
  int led_id = dist / STEP_RANGE + 0.5;
  
  if (led_id > 0 && led_id <= NUM_STEPS) {
    switch (SENSOR_ID) {
      case 1:
        return led_id;
      case 2:
        return NUM_STEPS - led_id + 1;
    }
  }

  return -1;
}

void set_led_strip(PubSubClient& client, int led_id, char* strip_intensity) {
  String topic = TOPIC_LED_STRIP + String(led_id);
  bool state = client.publish(topic.c_str(), strip_intensity, NUM_LEDS);

  if (state) {
    Serial.println("Message was published");
  }
  else {
    Serial.println("Message cannot be published");
  }
}

void black_strip(PubSubClient& client, int led_id) {
//  char strip_intensity[NUM_LEDS + 1]{0};
  set_led_strip(client, led_id, "\0");
}

void white_strip(PubSubClient& client, int led_id) {
  set_led_strip(client, led_id, "®®®®®®®®®®®®®®®®®®®®®®®®®®®®®®®®®®®®®®®®®®®®®");
}
