#pragma once

#include "Config.h";

#define MAX_RANGE 270.0
#define AVG_RANGE_NUMBER  100

const auto STEP_RANGE = MAX_RANGE / (NUM_STEPS + 1);


void range_init() {
  pinMode(PIN_TRIG, OUTPUT);
  pinMode(PIN_ECHO, INPUT);
}

float range() {
  digitalWrite(PIN_TRIG, LOW);
  delayMicroseconds(2);
  digitalWrite(PIN_TRIG, HIGH);
  delayMicroseconds(10);
  digitalWrite(PIN_TRIG, LOW);
  long duration = pulseIn(PIN_ECHO, HIGH);
  float distance = duration / 2. * 0.0343; // in cm
  
  return distance;
}

float avg_range() {
  float total_range = 0.0;

  for (int i = 0; i < AVG_RANGE_NUMBER; i++) {
    auto _range = range();

//    if (_range > MAX_RANGE) {
//      i--;
//      continue;
//    }
    total_range += (_range > MAX_RANGE) ? MAX_RANGE : _range;
//    total_range += _range;
  }

  return total_range / AVG_RANGE_NUMBER;
}
