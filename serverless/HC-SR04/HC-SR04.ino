#include "Config.h"
#include "WiFi.h"
#include "MQTT.h"
#include "Range.h"
#include "Leds.h"


int current_led_id = -1;

void blink(int);


void setup() {
  Serial.begin(9600);

  pinMode(LED_BUILTIN, OUTPUT);
  blink(3);

  WIFI_init(false);
  MQTT_init();
  range_init();
}

void loop() {
  mqtt_cli.loop();

  auto led_id = find_led(avg_range());
  if (current_led_id != led_id) {
    if (current_led_id != -1) black_strip(mqtt_cli, current_led_id);
    
    if (led_id != -1) white_strip(mqtt_cli, led_id);
    
    current_led_id = led_id;

    Serial.print("Led id: ");
    Serial.println(led_id);
    Serial.println();
  }

  delay(500);
  
//  mqtt_cli.publish(TOPIC_SEND_RANGE, String(round(range())).c_str());
//  delay(2000);
}

void blink(int n) {
  for (int i = 0; i < n; i++) {
    digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
    delay(500);
  }
}
