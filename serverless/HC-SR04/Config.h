#pragma once

String ssidAP = "ESP_WIFI"; // имя контроллера и точки доступа
String passwordAP = "ESP8266123"; // пароль точки доступа

const char* ssidCLI = "HONOR 20e";
const char* passwordCLI = "21e68c532791";

const char* MQTT_BROKER = "broker.emqx.io";
const int MQTT_PORT = 1883;

const int NUM_STEPS = 8;
const int NUM_LEDS = 150;

const int PIN_ECHO = 12; // D6
const int PIN_TRIG = 14; // D5

const int SENSOR_ID = 2;

// a topic where range data is sent
char* TOPIC_SEND_RANGE = "lab/leds/strip/range/2";
char* TOPIC_LED_STRIP = "lab/leds/strip/set_leds_bytes/";
